﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CandyLibrary;
namespace Present
{
    class PresentCreation
    {
        public static AbstractPresent CreatePresent()
        {
            AbstractPresent present = new Candies();

            Caramel caramel = new Caramel();
            caramel.Name = "Gosha";
            caramel.Weight = 5;
            caramel.Rigity = 50;
            present.MyPresent.Add(caramel);

            ChocolateBars chocolateBar = new ChocolateBars();
            chocolateBar.Name = "Black Panter";
            chocolateBar.Weight = 15;
            chocolateBar.Calories = 100;
            present.MyPresent.Add(chocolateBar);

            Lolipops lolipop = new Lolipops();
            lolipop.Name = "Petushok";
            lolipop.Weight = 25;
            lolipop.Shape = "Petuh";
            present.MyPresent.Add(lolipop);
            return present;
        }
    }
}
