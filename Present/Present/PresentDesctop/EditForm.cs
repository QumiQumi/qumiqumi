﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CandyLibrary;

namespace PresentDesctop
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
            MainForm mainForm = this.Owner as MainForm;
            List<Candies> candies = new List<Candies>{
            new Caramel{ClassName="Caramel"},
            new ChocolateBars{ClassName="ChocolateBars"},
            new ChocolateCandies{ClassName="ChocolateCandies"},
            new Lolipops{ClassName="Lolipops"},
            new SmallChocolateCandies{ClassName="SmallChocolateCandies"}
            };
            EditCandyChooseBox.DataSource = candies;
            EditCandyChooseBox.DisplayMember = "ClassName";
            EditCandyChooseBox.ValueMember = "Weight";
        }

        private void EditOkButton_Click(object sender, EventArgs e)
        {
            MainForm mainForm = this.Owner as MainForm;
            Candies candy = (Candies)EditCandyChooseBox.SelectedItem;
            if (EditNameBox.Text=="")
                candy.Name = "Unknown";
            else
                candy.Name = EditNameBox.Text;
            candy.Weight = Math.Abs(Convert.ToDouble(EditWeightBox.Text));
            MainForm.MainFormReference.DeleteCandy();
            MainForm.MainFormReference.AddCandy(candy);

            this.Hide();
            //Close();
        }

        private void EditCancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
