﻿namespace PresentDesctop
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EditCancelButton = new System.Windows.Forms.Button();
            this.EditOkButton = new System.Windows.Forms.Button();
            this.EditWeightBox = new System.Windows.Forms.TextBox();
            this.EditNameBox = new System.Windows.Forms.TextBox();
            this.EditCandyChooseBox = new System.Windows.Forms.ComboBox();
            this.EditWeightLbl = new System.Windows.Forms.Label();
            this.EditNameLbl = new System.Windows.Forms.Label();
            this.EditCandyLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // EditCancelButton
            // 
            this.EditCancelButton.Location = new System.Drawing.Point(147, 217);
            this.EditCancelButton.Name = "EditCancelButton";
            this.EditCancelButton.Size = new System.Drawing.Size(75, 23);
            this.EditCancelButton.TabIndex = 15;
            this.EditCancelButton.Text = "Cancel";
            this.EditCancelButton.UseVisualStyleBackColor = true;
            this.EditCancelButton.Click += new System.EventHandler(this.EditCancelButton_Click);
            // 
            // EditOkButton
            // 
            this.EditOkButton.Location = new System.Drawing.Point(66, 217);
            this.EditOkButton.Name = "EditOkButton";
            this.EditOkButton.Size = new System.Drawing.Size(75, 23);
            this.EditOkButton.TabIndex = 14;
            this.EditOkButton.Text = "OK";
            this.EditOkButton.UseVisualStyleBackColor = true;
            this.EditOkButton.Click += new System.EventHandler(this.EditOkButton_Click);
            // 
            // EditWeightBox
            // 
            this.EditWeightBox.Location = new System.Drawing.Point(66, 70);
            this.EditWeightBox.Name = "EditWeightBox";
            this.EditWeightBox.Size = new System.Drawing.Size(204, 22);
            this.EditWeightBox.TabIndex = 13;
            // 
            // EditNameBox
            // 
            this.EditNameBox.Location = new System.Drawing.Point(66, 42);
            this.EditNameBox.Name = "EditNameBox";
            this.EditNameBox.Size = new System.Drawing.Size(204, 22);
            this.EditNameBox.TabIndex = 12;
            // 
            // EditCandyChooseBox
            // 
            this.EditCandyChooseBox.FormattingEnabled = true;
            this.EditCandyChooseBox.Location = new System.Drawing.Point(66, 12);
            this.EditCandyChooseBox.Name = "EditCandyChooseBox";
            this.EditCandyChooseBox.Size = new System.Drawing.Size(204, 24);
            this.EditCandyChooseBox.TabIndex = 11;
            this.EditCandyChooseBox.Text = "(Choose candy) - click ->";
            // 
            // EditWeightLbl
            // 
            this.EditWeightLbl.AutoSize = true;
            this.EditWeightLbl.Location = new System.Drawing.Point(12, 73);
            this.EditWeightLbl.Name = "EditWeightLbl";
            this.EditWeightLbl.Size = new System.Drawing.Size(52, 17);
            this.EditWeightLbl.TabIndex = 10;
            this.EditWeightLbl.Text = "Weight";
            // 
            // EditNameLbl
            // 
            this.EditNameLbl.AutoSize = true;
            this.EditNameLbl.Location = new System.Drawing.Point(12, 45);
            this.EditNameLbl.Name = "EditNameLbl";
            this.EditNameLbl.Size = new System.Drawing.Size(45, 17);
            this.EditNameLbl.TabIndex = 9;
            this.EditNameLbl.Text = "Name";
            // 
            // EditCandyLbl
            // 
            this.EditCandyLbl.AutoSize = true;
            this.EditCandyLbl.Location = new System.Drawing.Point(12, 15);
            this.EditCandyLbl.Name = "EditCandyLbl";
            this.EditCandyLbl.Size = new System.Drawing.Size(48, 17);
            this.EditCandyLbl.TabIndex = 8;
            this.EditCandyLbl.Text = "Candy";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.EditCancelButton);
            this.Controls.Add(this.EditOkButton);
            this.Controls.Add(this.EditWeightBox);
            this.Controls.Add(this.EditNameBox);
            this.Controls.Add(this.EditCandyChooseBox);
            this.Controls.Add(this.EditWeightLbl);
            this.Controls.Add(this.EditNameLbl);
            this.Controls.Add(this.EditCandyLbl);
            this.Name = "EditForm";
            this.Text = "EditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EditCancelButton;
        private System.Windows.Forms.Button EditOkButton;
        private System.Windows.Forms.TextBox EditWeightBox;
        private System.Windows.Forms.TextBox EditNameBox;
        private System.Windows.Forms.ComboBox EditCandyChooseBox;
        private System.Windows.Forms.Label EditWeightLbl;
        private System.Windows.Forms.Label EditNameLbl;
        private System.Windows.Forms.Label EditCandyLbl;

    }
}