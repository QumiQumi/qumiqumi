﻿namespace PresentDesctop
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CandyLbl = new System.Windows.Forms.Label();
            this.NameLbl = new System.Windows.Forms.Label();
            this.WeightLbl = new System.Windows.Forms.Label();
            this.CandyChooseBox = new System.Windows.Forms.ComboBox();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.WeightBox = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CandyLbl
            // 
            this.CandyLbl.AutoSize = true;
            this.CandyLbl.Location = new System.Drawing.Point(12, 16);
            this.CandyLbl.Name = "CandyLbl";
            this.CandyLbl.Size = new System.Drawing.Size(48, 17);
            this.CandyLbl.TabIndex = 0;
            this.CandyLbl.Text = "Candy";
            // 
            // NameLbl
            // 
            this.NameLbl.AutoSize = true;
            this.NameLbl.Location = new System.Drawing.Point(12, 46);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(45, 17);
            this.NameLbl.TabIndex = 1;
            this.NameLbl.Text = "Name";
            // 
            // WeightLbl
            // 
            this.WeightLbl.AutoSize = true;
            this.WeightLbl.Location = new System.Drawing.Point(12, 74);
            this.WeightLbl.Name = "WeightLbl";
            this.WeightLbl.Size = new System.Drawing.Size(52, 17);
            this.WeightLbl.TabIndex = 2;
            this.WeightLbl.Text = "Weight";
            // 
            // CandyChooseBox
            // 
            this.CandyChooseBox.FormattingEnabled = true;
            this.CandyChooseBox.Location = new System.Drawing.Point(66, 13);
            this.CandyChooseBox.Name = "CandyChooseBox";
            this.CandyChooseBox.Size = new System.Drawing.Size(204, 24);
            this.CandyChooseBox.TabIndex = 3;
            this.CandyChooseBox.Text = "(Choose candy) - click ->";
            this.CandyChooseBox.SelectedIndexChanged += new System.EventHandler(this.CandyChooseBox_SelectedIndexChanged);
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(66, 43);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(204, 22);
            this.NameBox.TabIndex = 4;
            // 
            // WeightBox
            // 
            this.WeightBox.Location = new System.Drawing.Point(66, 71);
            this.WeightBox.Name = "WeightBox";
            this.WeightBox.Size = new System.Drawing.Size(204, 22);
            this.WeightBox.TabIndex = 5;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(66, 218);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(147, 218);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.WeightBox);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.CandyChooseBox);
            this.Controls.Add(this.WeightLbl);
            this.Controls.Add(this.NameLbl);
            this.Controls.Add(this.CandyLbl);
            this.Name = "AddForm";
            this.Text = "AddForm";
            this.Load += new System.EventHandler(this.AddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CandyLbl;
        private System.Windows.Forms.Label NameLbl;
        private System.Windows.Forms.Label WeightLbl;
        private System.Windows.Forms.ComboBox CandyChooseBox;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.TextBox WeightBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
    }
}