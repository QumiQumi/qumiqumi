﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CandyLibrary;

namespace PresentDesctop
{
    public partial class MainForm : Form
    {

        public static MainForm MainFormReference { get; set; }
        public AbstractPresent Podarok;
       // public static int listCount=1;
        public MainForm()
        {
            MainFormReference=this;
            InitializeComponent();
            
        }

        

        private void MainForm_Load(object sender, EventArgs e)
        {
            Podarok = Present.PresentCreation.CreatePresent();
            CalculatePresent();
            foreach (var item in Podarok.MyPresent)
            {
                listBox1.Items.Add(item.Name);
            }

         /*   listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Weight";
            AbstractPresent present = new Candies();*/
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CalculatePresent()
        {
            TotalWeightLbl.Text = "Weight = " + Convert.ToString(Podarok.GetWeight());
        }
        public void AddCandy(Candies candy) 
        {
            listBox1.Items.Add(candy.Name);
            Podarok.MyPresent.Add(candy);
            CalculatePresent();
        }
        public void DeleteCandy()
        {
            if (listBox1.SelectedIndex != -1)
            {
                int index = listBox1.SelectedIndex;
                listBox1.Items.RemoveAt(index);
                Podarok.MyPresent.RemoveAt(index);
            }
        }
        //нажатия кнопок
        private void AddButton_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                int index = listBox1.SelectedIndex;
                EditForm newFormEdit = new EditForm();
                newFormEdit.ShowDialog();
            }
            CalculatePresent();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DeleteCandy();
            CalculatePresent();

        }

        
    }
}
