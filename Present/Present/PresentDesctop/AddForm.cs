﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CandyLibrary;

namespace PresentDesctop
{

    public partial class AddForm : Form
    {

        public AddForm()
        {
            InitializeComponent();
            MainForm mainForm = this.Owner as MainForm;
            List<Candies> candies = new List<Candies>{
            new Caramel{Name="Tincker Bell", Weight=10, ClassName="Caramel"},
            new ChocolateBars{Name="Snickers", Weight=50, ClassName="ChocolateBars"},
            new ChocolateCandies{Name="Arabian night", Weight=20, ClassName="ChocolateCandies"},
            new Lolipops{Name="Petushok", Weight=35, ClassName="Lolipops"},
            new SmallChocolateCandies{Name="Chio-Rio", Weight=15, ClassName="SmallChocolateCandies"}
            };
            CandyChooseBox.DataSource = candies;
            CandyChooseBox.DisplayMember = "ClassName";
            CandyChooseBox.ValueMember = "Weight";
            
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            MainForm mainForm = this.Owner as MainForm;
            Candies candy = (Candies)CandyChooseBox.SelectedItem;
            if (NameBox.Text == "")
                candy.Name = "Unknown";
            else
                candy.Name = NameBox.Text;
            candy.Weight = Math.Abs(Convert.ToDouble(WeightBox.Text)); 

            MainForm.MainFormReference.AddCandy(candy);

            this.Hide();
            //Close();
        }

        private void CandyChooseBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Candies candy = (Candies)CandyChooseBox.SelectedItem;
            NameBox.Text = candy.Name;
            WeightBox.Text = Convert.ToString(candy.Weight);
            
        }

        private void AddForm_Load(object sender, EventArgs e)
        {
        }
    }
}
