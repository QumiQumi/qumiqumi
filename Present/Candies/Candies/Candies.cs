﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Candies
{
    public class Candies{
        private string _name;
        private double _weight;

        public string Name
        {
            get { return _name; }
            set { _name = value;}
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        } 

}

}
