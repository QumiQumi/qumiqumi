﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandyLibrary
{
    public class Candies :AbstractPresent
    {
        private string _name;
        private double _weight;
        public string className;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }
       /* public override void Display()
        {
            Console.WriteLine("name:" +Name +"weight:" +Weight);
        }*/

    }
    
}
