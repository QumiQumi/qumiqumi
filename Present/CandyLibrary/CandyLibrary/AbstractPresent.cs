﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandyLibrary
{
    abstract public class AbstractPresent
    {
        public static double PresentWeight { get; set; }
        public List<Candies> MyPresent = new List<Candies>();
        public double GetWeight()
        {
            PresentWeight = 0;
            foreach (var candy in MyPresent)
            {
                PresentWeight += candy.Weight;
            }
            return PresentWeight;

        }
       // public abstract void Display();
    }
}
